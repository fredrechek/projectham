



$(document).ready(function () {
  // SLIDER
  $('.slider-top').slick({
    dots: true,
    customPaging: function (slick, index) {
      let targetImage = slick.$slides.eq(index).find('img').attr('src');
      return `<img src="${targetImage}"/>`;
    },
    prevArrow: $('.prev'),
    nextArrow: $('.next')
  });


  // TABS
  $(".tabs").on("click", handleTabs)
  const articles = document.querySelectorAll('.tabs-article');
  function handleTabs(event) {
    const newArticles = Array.from(articles);
    for (const el of this.children) {
      el.classList.remove('tabs-title-active');
    }
    event.target.classList.toggle('tabs-title-active');
    newArticles.forEach(element => {
      element.classList.remove('tabs-article-active');
      if (element.dataset.id === event.target.dataset.id) {
        element.classList.add('tabs-article-active');
      }
    });
  };

  // WORK FILTER
  const filterTrigger = document.querySelector('.trigger-list');
  const filterList = document.querySelector('.portfolio');
  const addItem = document.querySelector('#loadMore');
  const work = document.querySelector('.work');

  const filterCards = [
    {
      img: './style/img/graphic_design/graphic-design5.jpg',
      dataItem: 'graphicDesing',
      filterValue: 'creative design',
      filterSubtext: 'Web Design'
    },
    {
      img: './style/img/graphic_design/graphic-design7.jpg',
      dataItem: 'graphicDesing',
      filterValue: 'creative design',
      filterSubtext: 'Web Design'
    },
    {
      img: './style/img/graphic_design/graphic-design9.jpg',
      dataItem: 'graphicDesing',
      filterValue: 'creative design',
      filterSubtext: 'Web Design'
    },
    {
      img: './style/img/landing_page/landing-page1.jpg',
      dataItem: 'landingPages',
      filterValue: 'creative design',
      filterSubtext: 'Landing Page'
    },
    {
      img: './style/img/landing_page/landing-page2.jpg',
      dataItem: 'landingPages',
      filterValue: 'creative design',
      filterSubtext: 'Landing Page'
    },
    {
      img: './style/img/landing_page/landing-page3.jpg',
      dataItem: 'landingPages',
      filterValue: 'creative design',
      filterSubtext: 'Landing Page'
    },
    {
      img: './style/img/landing_page/landing-page4.jpg',
      dataItem: 'landingPages',
      filterValue: 'creative design',
      filterSubtext: 'Landing Page'
    },
    {
      img: './style/img/landing_page/landing-page7.jpg',
      dataItem: 'landingPages',
      filterValue: 'creative design',
      filterSubtext: 'Landing Page'
    },
    {
      img: './style/img/wordpress/wordpress4.jpg',
      dataItem: 'wordpress',
      filterValue: 'creative design',
      filterSubtext: 'wordpress'
    },
    {
      img: './style/img/wordpress/wordpress5.jpg',
      dataItem: 'wordpress',
      filterValue: 'creative design',
      filterSubtext: 'wordpress'
    },
    {
      img: './style/img/wordpress/wordpress6.jpg',
      dataItem: 'wordpress',
      filterValue: 'creative design',
      filterSubtext: 'wordpress'
    },
    {
      img: './style/img/wordpress/wordpress8.jpg',
      dataItem: 'wordpress',
      filterValue: 'creative design',
      filterSubtext: 'wordpress'
    },
  ];



  const addItems = function (list) {
    addItem.remove();
    const loader = document.createElement('div');
    const gif = document.createElement('img');
    gif.src = './style/icons/91.gif';
    loader.classList.add('loader');
    loader.prepend(gif);
    work.append(loader);
    setTimeout(function () {
      loader.remove();
      list.forEach(element => {
        filterList.insertAdjacentHTML('beforeend', element);
      });
      doFilter();
    }, 1000);
  };

  const createFilterCard = function (event) {
    event.preventDefault();
    const item = filterCards.map(element => {
      let card = `
    <div class="portfolio-item" data-item="${element.dataItem}">
      <img src="${element.img}" alt="" class="portfolio-img">
      <div class="portfolio-text">
        <div class="portfolio-action">
          <a href="#" class="portfolio-link"><i class="icon-link"></i></a>
          <a href="#" class="portfolio-link"><i class="icon-square"></i></a>
        </div>
        <p class="portfolio-value">${element.filterValue}</p>
        <p class="portfolio-name">${element.filterSubtext}</p>
      </div>
    </div>
    `;
      return card;
    });
    addItems(item);
  };

  const doFilter = function () {
    const triggerElement = Array.from(filterTrigger.children).filter(element => element.classList.contains('filter-item--active'));
    const listArray = Array.from(filterList.children);

    if (triggerElement[0].dataset.filter === 'all') {
      listArray.forEach(element => {
        element.style.cssText = 'display: inline-block';
      });
    } else {
      listArray.forEach(element => element.style.cssText = 'display: inline-block');
      listArray.filter(item => {
        if (item.dataset.item !== triggerElement[0].dataset.filter) {
          return item.style.cssText = 'display: none';
        }
      });
    }
  };

  const handleTriger = function (event) {
    console.log(event.target);
    for (const el of this.children) {
      el.classList.remove('filter-item--active');
    }
    event.target.classList.toggle('filter-item--active');
    doFilter();
  };

  addItem.addEventListener('click', createFilterCard);
  filterTrigger.addEventListener('click', handleTriger);
})

